require 'sinatra'
require 'nokogiri'
require 'open-uri'
require 'csv'


#Setup URL's To Scrape
global_urls = ['urls_you_want_to_scrape']
#Begin App
def scrape(urls)
	urls.each do |url|
		data = Nokogiri::HTML(open(url)) # Opens URL
		#Parent CSS Class
		articles = data.css('.article')
		CSV.open("articles.csv", "a+",skip_blanks: true) do |csv|
			articles.each do |article|
			    #Individual sections to scrape
				title = article.at_css(".article_title").text
				cta = "https://www.example.com" << article.at_css('.article_cta').attr('href')
				image = "https://www.example.com" << article.at_css('.article_img').attr('src')
				copy = article.at_css('.article').text
				csv << [title, cta, image, copy]
			end
		end 
	end
end

class Article <
	Struct.new(:title, :cta, :image,:copy,:text)
end

get '/' do
	#Check age of data file and refresh after a duration
	csv_last_edit = File.mtime("articles.csv")
	csv_yesterday = Time.now - (3600 * 24 * 2)
	if csv_last_edit < csv_yesterday
		File.delete("articles.csv")
		scrape(global_urls)
	end
	#Prepare search term to pull content
	@searched_articles = Array.new

	current_location = request.fullpath
	search_term = current_location.partition("=").last
	if search_term.include?("s")
		search_term.gsub!('s','')
	end
	search_term.downcase!
	term_array = search_term.split("+")
	# Open the csv file
	f = File.open("articles.csv", "r")
	f.each_line { |line|
		fields = line.split(',')
		if fields[0].nil? || fields[0].empty? || fields[2].nil? || fields[2].empty?
		else
			a = Article.new
			a.title = fields[0]
			a.title.tr!('"','')
			a.cta = fields[1]
			a.image = fields[2]
			a.copy = fields[3]
			title_val = a.title
			copy_val = a.copy
			image_val = a.image
			search_val = copy_val + title_val
			if term_array.any? { |word| search_val.downcase.include?(word) }
				if image_val.include?("jpg")
					@searched_articles.push(a)
				end
			end
		end
	}
	@searched_articles = Hash[*@searched_articles.map {|obj| [obj.title, obj]}.flatten].values
	if @searched_articles.length == 0
		@searched_articles = "Empty"
	end
	erb :shows
